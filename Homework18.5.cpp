#include <iostream>
#include <string>

class Player
{
private:
	std::string name;
	int score;

public:
	Player() : name(), score(0)
	{}
	Player(const std::string& name, int score) : name(name), score(score)
	{}
	const std::string& getName() const
	{
		return name;
	}
	int getScore() const
	{
		return score;
	}
};

void sortPlayers(Player* players, int numPlayers)
{
	for (int i = 0; i < numPlayers - 1; i++)
	{
		for (int j = 0; j < numPlayers - i - 1; j++)
		{
			if (players[j].getScore() < players[j + 1].getScore())
			{
				std::swap(players[j], players[j + 1]);
			}
		}
	}
}

int main()
{
	int numPlayers;
	std::cout << "Enter the number of players: ";
	std::cin >> numPlayers;

	Player* players = new Player[numPlayers];

	for (int i = 0; i < numPlayers; i++)
	{
		std::string name;
		int score;

		std::cout << "Enter the name of player " << i + 1 << ": ";
		std::cin >> name;
		std::cout << "Enter the score of player " << i + 1 << ": ";
		std::cin >> score;

		players[i] = Player(name, score);
	}

	sortPlayers(players, numPlayers);

	std::cout << "\nSorted Players;\n";
	for (int i = 0; i < numPlayers; i++)
	{
		std::cout << players[i].getName() << ": " << players[i].getScore() << std::endl;
	}

	delete[] players;

	return 0;
}
